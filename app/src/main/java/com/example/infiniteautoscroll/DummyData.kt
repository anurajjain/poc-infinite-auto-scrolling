package com.example.infiniteautoscroll

data class DummyData(
    val name : String,
    val imageUrl : String
)