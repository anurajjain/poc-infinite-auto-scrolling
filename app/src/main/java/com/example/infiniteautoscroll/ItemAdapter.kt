package com.example.infiniteautoscroll

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.infiniteautoscroll.databinding.InnerRvItemBinding

class ItemAdapter : RecyclerView.Adapter<ItemAdapter.VH>() {

    class VH(private val binding: InnerRvItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item : DummyData){
            binding.apply {
                Glide.with(root.context)
                    .load(item.imageUrl)
                    .placeholder(R.color.grey)
                    .error(R.drawable.ic_placeholder)
                    .into(img)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(
            InnerRvItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size

    private val differCallback = object : DiffUtil.ItemCallback<DummyData>(){
        override fun areItemsTheSame(oldItem: DummyData, newItem: DummyData): Boolean {
            return  oldItem.imageUrl == newItem.imageUrl
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: DummyData, newItem: DummyData): Boolean {
            return oldItem == newItem
        }

    }

    val differ = AsyncListDiffer(this,differCallback)
}