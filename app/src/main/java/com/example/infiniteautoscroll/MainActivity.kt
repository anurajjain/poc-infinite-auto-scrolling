package com.example.infiniteautoscroll

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.infiniteautoscroll.databinding.ActivityMainBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private val data: ArrayList<ArrayList<DummyData>> = arrayListOf()
    private lateinit var binding: ActivityMainBinding
    private val rowsAdapter: RowsAdapter by lazy { RowsAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        createDummyList()
        setViews()
        setListener()
    }

    private fun setListener() {
        rowsAdapter.setListener { recyclerView, itemAdapter ->
            setAutoScrolling(recyclerView, itemAdapter)
        }
    }

    private fun setViews() {
        rowsAdapter.setList(data)
        binding.rv.adapter = rowsAdapter
        binding.runnableText.apply {
            text = getString(R.string.text_data)
            isSelected = true
        }
    }

    private fun createDummyList() {
        // random image generator url
        val url = "https://picsum.photos/200/200/?random?temp="
        val list = arrayListOf<DummyData>()
        for (i in 1..4) {
            list.clear()
            for (j in 1..10) {
                list.add(DummyData("img $i.$j", "${url}$j"))
            }
            data.add(list)
        }
    }

    private fun setAutoScrolling(recycler: RecyclerView, adapter: ItemAdapter) {
        lifecycleScope.launch {
            autoScrollItemList(recycler, adapter)
        }
    }

    private tailrec suspend fun autoScrollItemList(recycler: RecyclerView, adapter: ItemAdapter) {
        if (recycler.canScrollHorizontally(DIRECTION_RIGHT)) {
            recycler.smoothScrollBy(SCROLL_DX, 0)
        } else {
            val firstPosition =
                (recycler.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
            if (firstPosition != RecyclerView.NO_POSITION) {
                val currentList = adapter.differ.currentList
                val secondPart = currentList.subList(0, firstPosition)
                val firstPart = currentList.subList(firstPosition, currentList.size)
                adapter.differ.submitList(firstPart + secondPart)
            }
        }
        delay(DELAY_BETWEEN_SCROLL_MS)
        autoScrollItemList(recycler, adapter)
    }

    companion object {
        private const val DELAY_BETWEEN_SCROLL_MS = 25L
        private const val SCROLL_DX = 5
        private const val DIRECTION_RIGHT = 1
    }
}