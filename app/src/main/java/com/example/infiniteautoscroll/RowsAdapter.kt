package com.example.infiniteautoscroll

import android.content.res.Resources
import android.graphics.Rect
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.infiniteautoscroll.databinding.DummyItemBinding


class RowsAdapter : RecyclerView.Adapter<RowsAdapter.VH>() {

    private var list : ArrayList<ArrayList<DummyData>> = arrayListOf()
    private lateinit var listener : (RecyclerView, ItemAdapter) -> Unit

    fun setList(data : ArrayList<ArrayList<DummyData>>){
        this.list = data
        notifyDataSetChanged()
    }

    class VH(private val binding: DummyItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(list: ArrayList<DummyData>, listener: (RecyclerView, ItemAdapter) -> Unit){
            val itemAdapter = ItemAdapter()
            itemAdapter.differ.submitList(list)
            binding.innerRv.setOnTouchListener(OnTouchListener { v, event -> true })
            binding.innerRv.apply {
                adapter = itemAdapter
                if (itemDecorationCount == 0) {
                    val itemDecoration = object : RecyclerView.ItemDecoration() {
                        override fun getItemOffsets(
                            outRect: Rect,
                            view: View,
                            parent: RecyclerView,
                            state: RecyclerView.State
                        ) {
                            outRect.left =
                                if (getChildAdapterPosition(view) == 0 && adapterPosition%2 == 0) getPxFromDp(
                                    resources,
                                    16f
                                )
                                else if(getChildAdapterPosition(view) == 0 && adapterPosition%2 != 0) getPxFromDp(
                                    resources,
                                    66f
                                ) else 0
                            outRect.right = getPxFromDp(resources, 16f)
                        }
                    }
                    addItemDecoration(itemDecoration)
                }
            }
            listener.run {
                this(binding.innerRv, itemAdapter)
            }
        }

        private fun getPxFromDp(resources: Resources, dpValue: Float): Int {
            return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dpValue,
                resources.displayMetrics
            ).toInt()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(
            DummyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position], listener)
    }

    override fun getItemCount(): Int = list.size

    fun setListener(listener : (RecyclerView, ItemAdapter) -> Unit){
        this.listener = listener
    }
}